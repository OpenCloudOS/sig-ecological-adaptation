# 【生态适配SIG】   
## 职责流程 Process  
[适配流程说明](/doc/adaptation-process.md)
## 职责范围 Scope   
致力于在OpenCloudOS社区配适兼容认证上下游软硬件厂商，保持OpenCloudOS社区生态的活跃。   

## SIG成员 Members   
李星辉（腾讯）  
宁固（中科方德）  
余阳（腾讯）
彭浩（腾讯）
徐姗姗（中科方德）  
王海峰（E企研究院）
张广彬（E企研究院）
## 联络方式 Contact  
- 微信群   
- 邮件列表  
    - 小组邮件列表  
      -  sig-ecological-adaptation@lists.opencloudos.org
## 会议制度 Meetings  
本兴趣小组的邮件列表会即时公布本小组的各项会议的具体细节。欢迎订阅我们的邮件列表。  
- 会议   
    - 会议周期：双周例会  
    - 会议链接：（请填写）  
- 定期沙龙（建议试行月度线上沙龙）  
## 如何加入SIG并参与贡献：  
1. 注册Gitee/GitHub账号  
2. 签署CLA  
3. 找到对应SIG项目仓库地址  
4. 参与贡献  
## SIG规划 Roadmap
(具体Roadmap需要依据硬件平台的Roadmap)  
- 首先需要保证新硬件平台的正常运行
- 其次要对新平台的Feature进行开发